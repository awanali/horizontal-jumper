﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

    [SerializeField]
    private Text _scoreText;

    [SerializeField]
    private AudioClip _hitSound, _jumpSound, _scoreSound;

    private Rigidbody2D _player;

    private int _score = 0;

    private AudioSource _audioSource;

    // Start is called before the first frame update
    void Start()
    {
        _player = transform.GetComponent<Rigidbody2D>();
        _audioSource = transform.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            // Play jump sound
            _audioSource.PlayOneShot(_jumpSound);
            // Jump
            _player.velocity = new Vector2(0, 7f);
        }
    }

    void OnCollisionEnter2D()
    {
        // Play hit sound
        _audioSource.PlayOneShot(_hitSound);
        // Reload scene to reset game
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        // Play score sound
        _audioSource.PlayOneShot(_scoreSound);
        // Increase score
        _score++;
        // Update score text
        _scoreText.text = _score.ToString();
    }
}
