﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject _obstacle;

    private int _timer = 100;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Increase Timer
        if (_timer <= 100)
        {
            _timer++;
            return;
        }

        // Reset timer
        _timer = 0;
        // Instantiate Obstacle
        GameObject newObstacle = Instantiate(_obstacle, new Vector2(_obstacle.transform.position.x, _obstacle.transform.position.y + Random.Range(-2, 2)), _obstacle.transform.rotation);
        // Destroy Obstacle
        Destroy(newObstacle, 5);

    }
}
